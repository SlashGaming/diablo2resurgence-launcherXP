This is a launcher for launching 1.13c/d versions of Diablo 2 and accessing the reddit servers (slashdiablo, resurgence).

-----------


The launcher allows you to select which "version of the game" you want to launch (SlashDiablo Resurgence, SlashDiablo, Battle.net), as well as define which arguments you want to run the game with. Please be aware that connecting to b.net will update your client to 1.14 which may not be easily rolled back to 1.13!
 
Additionally, the launcher allows you to specify the directory of your BH (the maphack that works with this mod) to easily launch it after you have launched your Diablo game. Lastly, if you have the multimonitor .dll install you can also launch multiple instances of the game from the launcher.
This is a beta version of the launcher so it is expected that there will be bugs!
  
Bug reports:
Please post all bugs to the gitlab issue page and make sure to label them with the "Launcher" label.
 
General Instructions:
Download the .exe to a directory of your choosing.
Once the launcher has booted it will attempt to find an installed diablo 2 instance through the registry. If it cannot do so it will ask you to specify a directory. You should check the directory to make sure that it's pointing to the one where you want the Resurgence mod installed (click on "set path" next to Diablo in the launcher to see what directory it's currently pointing to).
The launcher will make copies of both the default 1.13c/1.13d MPQ as well as the Resurgence MPQ and hot swap between them depending on which version you have chosen. Additionally, it will look at the public gitlab to determine whether a new patch has been released. If one is found, it will ask you to download it before launching Resurgence.
BH is not found automatically. If you want to launch BH with the launcher you will have to manually set a path.
The launcher is based on a largely modified version of this open source launcher.
 
Important Note regarding 1.14a
If you have clean installed Diablo using the 1.14a launcher you cannot roll back to 1.13c. You must find an older version of diablo and then update it to 1.13c.
If you have upgraded Diablo to 1.14 through the automatic patching process, you can roll back to 1.13c. Follow the instructions here: http://forum.median-xl.com/viewtopic.php?t=1181
